  //เสกกล้วย--------------------------------------------------
  function UpArrow() {
    upArrow = new Sprite(scene, "sizeup.png", 60, 60);

    upArrow.reset = function () {
        this.setDY((Math.random() * 10) + 5);
        this.setDX(0);
        newX = Math.random() * scene.width;
        this.setPosition(newX, 50);
    }

    upArrow.checkBounds = function () {
        if (this.y > scene.height) {
            this.reset();
        }
    }

    upArrow.reset();

    return upArrow;
}

function makeBomb() {
    upArrows = new Array(NUM_BombS);
    for (i = 0; i < NUM_BombS; i++) {
        upArrows[i] = new UpArrow();
    }
}

function updateBombs() {
    for (i = 0; i < NUM_BombS; i++) {
        upArrows[i].update();
    }
}